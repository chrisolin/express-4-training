var exampleController = require('./controllers/example');

module.exports = function (app) {
    app.get('/', exampleController.myController);
	app.get('/break', exampleController.fiveHundred);
};